'use strict';

const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const async = require('async');
const jsonfile = require('jsonfile');
const bunyan = require('bunyan');
const shelljs = require('shelljs');
const i18next = require('i18next');
const i18Backend = require('i18next-node-fs-backend');

const log = bunyan.createLogger({name: 'CAPP3', Model: 'Language'});

module.exports = function(Language) {
  let app = null;
  Language.on('attached', (application) => {
    app = application;
    app.i18next = i18next;
    app.on('started-disabled', () => {
      let locales = {};
      app.models.Attack.find().then(attacks => {
        _.each(attacks, (attack) => {
          let attackLocales = shelljs.ls('-d', path.join('node_modules', attack.module, 'locales') + path.sep + '*');
          _.each(attackLocales, (al) => {
            let sAL = al.split(path.sep);
            locales[_.last(sAL)] = true;
          });
        });
        async.each(_.keys(locales), (locale, lcb) => {
          let outFile = path.join('locales', locale, 'attacks.json');
          let content = {};
          _.each(attacks, (attack) => {
            let localeFile = path.resolve(path.join('node_modules', attack.module, 'locales', locale, 'attacks.json'));
            if (shelljs.test('-e', localeFile)) {
              _.merge(content, require(localeFile));
            }
          });
          fs.writeFile(outFile, JSON.stringify(content, null, 2), 'utf8', lcb);
        }, (err) => {
          if (err) {
            log.error(err);
          } else {
            log.info(`Attack Locales Updated: ${JSON.stringify(_.keys(locales))}`);
            i18next.use(i18Backend).init({
              load: 'currentOnly',
              preload: _.keys(locales),
              ns: ['common'],
              defaultNS: 'common',
              fallbackLng: 'en-US',
              backend: {
                loadPath: 'locales/{{lng}}/{{ns}}.json',
                addPath: 'locales/{{lng}}/{{ns}}.missing.json',
                jsonIndent: 2,
              },
            });
          }
        });
      }).catch(err => {
        log.error(err);
      });
    });
  });

  /**
   * Get i18n Language File
   * @param {string} lng Language
   * @param {string} nsFile Namespace
   * @param {object} res Response object
   * @param {Function(Error, object)} callback
   */
  Language.getLanguageFile = function(lng, nsFile, res, callback) {
    let me = this;
    switch (nsFile) {
      case 'attacks.json':
      case 'common.json':
        let localeFile = path.resolve('locales' + path.sep + lng + path.sep + nsFile);
        if (shelljs.test('-f', localeFile)) {
          res.sendFile(localeFile);
        } else {
          callback(null, {});
        }
        break;
      default:
        let ns = nsFile.split('.');

        let sLngs = lng.split('+');
        let sNSs = ns[0].split('+');
        let lngTasks = {};
        _.each(sLngs, (sLng) => {
          lngTasks[sLng] = (cb) => {
            let nsTasks = {};
            _.each(sNSs, (sNS) => {
              nsTasks[sNS] = (nsCb) => {
                let serverModule = _.find(app.modules.server(), (serverModule) => {
                  return serverModule.config.locale === sNS;
                });
                if (!_.isNull(serverModule)) {
                  let localeFile = path.resolve(`${serverModule.directory}${path.sep}locales${path.sep}${sLng}${path.sep}${sNS}.json`);
                  if (shelljs.test('-f', localeFile)) {
                    jsonfile.readFile(localeFile, nsCb);
                  } else {
                    nsCb(null, {});
                  }
                } else {
                  nsCb(null, {});
                }

              }
            });
            async.series(nsTasks, cb);
          }
        });
        async.series(lngTasks, callback);
    }
  };
};
