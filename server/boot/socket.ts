import * as _ from 'lodash';
import * as moment from 'moment';
import {EventEmitter} from "events";
import {BootScript} from "@mean-expert/boot-script";
import {AccessToken, LoopBackApplication, Registry, User, Role} from "loopback";
import {Socket} from "socket.io";
const LoopBackSocket = require('loopback-socket');
import * as SocketIORedis from "socket.io-redis";
let log = require('@dosarrest/loopback-component-logger')('server/boot/authentication');

interface LoopBackSocket extends Socket {
  user: User;
  account?: any;
  accessToken: AccessToken;
  auth: (fn: any) => any;
  onConnected: (fn: any) => any;
}

@BootScript()
class SocketIO extends EventEmitter {
  app: LoopBackApplication;
  socket: LoopBackSocket;
  constructor(app: LoopBackApplication) {
    super();
    let me = this;
    me.app = app;
    let authOptions = app.get('auth');
    (app as any).socketio = me;
    me.socket = LoopBackSocket.get('app');
    app.on('started', () => {
      let socketConfig = me.app.get('socket');
      if (!_.isUndefined(socketConfig) && !_.isUndefined(socketConfig.redisOptions) && socketConfig.redisOptions.enabled === true) {
        delete socketConfig.redisOptions.enabled;
        (me.socket as any)._io.adapter(SocketIORedis(socketConfig.redisOptions));
      }
    });
    if (authOptions.enabled === true) {
      me.socket.auth(me.authenticateSocket.bind(me));
    }
  }
  onAuthSuccess(socket: Socket | any) {
    let me = this;
    socket.on('watch', me.onWatch.bind(me, socket));
    socket.on('unwatch', me.onUnwatch.bind(me, socket));
    socket.on('disconnect', me.onDisconnect.bind(me, socket));
    socket.tokenChecker = setInterval(me.checkToken.bind(me, socket), 60 * 1000);
    let authOptions = me.app.get('auth');
    if (authOptions.enabled === true) {
      socket.join(`user-${socket.user.id}`);
      if (socket.account) { socket.join(`account-${socket.account.id}`); }
      me.emit('authenticated', socket);
    }
  }
  onWatch(socket: any, component: string, item: string) {
    let me = this;
    let authOptions = me.app.get('auth');
    if (_.isUndefined(item)) {
      if (authOptions.enabled === true) {
        log.info(`${socket.user.username}(${socket.id}) watching ${component}`);
        socket.join(`user-${socket.user.id}-${component}`);
        if (socket.account) {
          socket.join(`account-${socket.account.id}-${component}`);
        }
      } else {
        log.info(`${socket.id} watching ${component}`);
        socket.join(`${component}`);
      }
    } else {
      if (authOptions.enabled === true) {
        log.info(`${socket.user.username}(${socket.id}) watching ${component} - ${item}`);
        socket.join(`user-${socket.user.id}-${component}-${item}`);
        if (socket.account) {
          socket.join(`account-${socket.account.id}-${component}-${item}`);
        }
      } else {
        log.info(`${socket.id} watching ${component} - ${item}`);
        socket.join(`${component}-${item}`);
      }
    }
    if  (authOptions.enabled === true && socket.user) {
      socket.user.roles.find().then((roles: Role[]) => {
        if (_.find(roles, {name: 'Administrator'}) !== undefined) {
          socket.join(`${component}`);
        }
      }).catch((err: Error) => {
        log.error(err);
      });
    }
  }
  onUnwatch(socket: any, component: string, item: string) {
    let me = this;
    let authOptions = me.app.get('auth');
    if (_.isUndefined(item)) {
      if (authOptions.enabled === true) {
        log.info(`${socket.user.username}(${socket.id}) unwatching ${component}`);
        socket.leave(`user-${socket.user.id}-${component}`);
        if (socket.account) {
          socket.leave(`account-${socket.account.id}-${component}`);
        }
      } else {
        log.info(`${socket.id} unwatching ${component}`);
        socket.leave(`${component}`);
      }
    } else {
      if (authOptions.enabled === true) {
        log.info(`${socket.user.username}(${socket.id}) unwatching ${component} - ${item}`);
        socket.leave(`user-${socket.user.id}-${component}-${item}`);
        if (socket.account) {
          socket.leave(`account-${socket.account.id}-${component}-${item}`);
        }
      } else {
        log.info(`${socket.id} unwatching ${component} - ${item}`);
        socket.leave(`${component}-${item}`);
      }
    }
    if  (authOptions.enabled === true && socket.user) {
      socket.user.roles.find().then((roles: Role[]) => {
        if (_.find(roles, {name: 'Administrator'}) !== undefined) {
          socket.leave(`${component}`);
        }
      }).catch((err: Error) => {
        log.error(err);
      });
    }
  }
  onDisconnect(socket: any, reason: string) {
    let me = this;
    if (socket.tokenChecker) {
      clearInterval(socket.tokenChecker);
    }
    let authOptions = me.app.get('auth');
    if (authOptions.enabled === true) {
      let accessToken = socket.request._query.accessToken || socket.request.signedCookies.accessToken;
      if (_.isUndefined(accessToken)) { accessToken = '0'; }
      log.info({socket_id: socket.id, user: `${socket.user.username}(${socket.user.id})`, accessToken: accessToken}, `Disconnected Reason: ${reason}`);
    } else {
      log.info({socket_id: socket.id}, `Disconnected Reason: ${reason}`);
    }
  }
  checkToken(socket: any) {
    let me = this;
    let authOptions = me.app.get('auth');
    if (authOptions.enabled === true) {
      let accessToken = socket.request._query.accessToken || socket.request.signedCookies.accessToken;
      if (_.isUndefined(accessToken)) { accessToken = '0'; }
      let registry: Registry = (me.app as any).registry;
      let AccessToken: any = (registry as any).getModelByType('AccessToken');
      AccessToken.findById(accessToken).then((at: AccessToken) => {
        if (at === null) {
          log.info({socket_id: socket.id, user: `${socket.user.username}(${socket.user.id})`, accessToken: accessToken}, 'AccessToken Expired');
          socket.emit('logout');
          setTimeout(() => {
            socket.disconnect(true);
          }, 1000);
        } else {
          let created = moment(at.created).add(at.ttl, 's');
          let expiry = created.diff(moment(), 'minutes', true);
          if (expiry <= 2) {
            log.info({socket_id: socket.id, user: `${socket.user.username}(${socket.user.id})`, accessToken: accessToken, expiry: `in ${expiry} minutes, forcing logout`});
            socket.emit('logout');
            setTimeout(() => {
              socket.disconnect(true);
            }, 1000);
          } else {
            log.info({socket_id: socket.id, user: `${socket.user.username}(${socket.user.id})`, accessToken: accessToken, expiry: `in ${expiry} minutes`});
          }
        }
      }).catch((err: Error) => {
        log.info({socket_id: socket.id, user: `${socket.user.username}(${socket.user.id})`, accessToken: accessToken, error: err});
        socket.emit('logout');
        setTimeout(() => {
          socket.disconnect(true);
        }, 1000);
      });
    }
  }
  authenticateSocket(socket: any, credentials: any, cb: any): any {
    let me = this;
    let authOptions = me.app.get('auth');
    if (authOptions.enabled === true) {
      let accessToken = socket.request._query.accessToken || socket.request.signedCookies.accessToken;
      if (_.isUndefined(accessToken)) { accessToken = '0'; }
      let registry: Registry = (me.app as any).registry;
      let AccessToken: any = (registry as any).getModelByType('AccessToken');
      let User: any = (registry as any).getModelByType('User');
      if (User.relations.account) {
        AccessToken.findById(accessToken, {include: [{user: ['account', 'roles']}]}).then((model: AccessToken) => {
          if (_.isNull(model)) {
            setTimeout(() => {
              log.info({socket_id: socket.id, accessToken: accessToken}, 'Access Token Not Found');
              socket.disconnect(true);
            }, 500);
            // throw new Error('Access Token Not Found');
            cb(new Error('Access Token Not Found'), false);
            return null;
          } else {
            socket.user = (model as any).user();
            socket.account = (socket.user as any).account();
            socket.accessToken = model;
            log.info({socket_id: socket.id, accessToken: accessToken}, `Authenticated as (${socket.account.name})${socket.user.username}`);
            me.onAuthSuccess.call(me, socket);
            cb(null, true);
            return socket.user;
          }
        }).catch((err: any) => {
          log.error(err);
          cb(err);
        });
      } else {
        AccessToken.findById(accessToken, {include: {user: ['roles']}}).then((model: AccessToken) => {
          if (_.isNull(model)) {
            setTimeout(() => {
              log.info({socket_id: socket.id, accessToken: accessToken}, 'Access Token Not Found');
              socket.disconnect(true);
            }, 500);
            // throw new Error('Access Token Not Found');
            cb(new Error('Access Token Not Found'), false);
            return null;
          } else {
            socket.user = (model as any).user();
            socket.accessToken = model;
            log.info({socket_id: socket.id, accessToken: accessToken}, `Authenticated as ${socket.user.username}`);
            me.onAuthSuccess.call(me, socket);
            cb(null, true);
            return socket.user;
          }
        }).catch((err: any) => {
          log.error(err);
          cb(err);
        });
      }
    } else {
      cb(null, true);
    }
  }
}
module.exports = SocketIO;
