Ext.define('Client.model.Account', {
  extend: 'Ext.data.Model',
  requires: [
    'Ext.data.field.Date',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json'
  ],
  fields: [{
    name: 'id',
    persist: false
  }, {
    name: 'name'
  }, {
    name: 'description'
  }, {
    name: 'parentId'
  }, {
    name: 'type'
  }, {
    type: 'date',
    name: 'created'
  }]
});
