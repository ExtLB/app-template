Ext.define('Client.controller.Navigation', {
  extend: 'Ext.app.Controller',
  requires: [
    'Ext.Promise',
    'Ext.Ajax',
    'Ext.route.Route',
    'Ext.util.History'
  ],
  routes: {
    // ':{view}(/:{page})(/:{arg0})(/:{arg1})(/:{arg2})': {action: 'viewHandler'},
    ':{perspective}(/:{page})(/:{arg0})(/:{arg1})(/:{arg2})': {action: 'perspectiveRouter'},
  },
  listen: {
    global: {
      unmatchedroute: 'onUnmatchedRoute'
    }
  },
  defaultToken: '',
  perspectiveTokens: {},
  viewHandler: (params) => {
    let me = this;
    try {
      let Navigation = Client.app.getController('Navigation');
      let initiated = true;
      let viewType = Ext.ClassManager.getByAlias(`widget.${params.view}view`);
      if (!_.isUndefined(viewType)) {
        let mainView = Ext.Viewport.down(`${params.view}view`);
        if (_.isNull(mainView)) {
          initiated = false;
          Ext.Viewport.add({xtype: `${params.view}view`});
          mainView = Ext.Viewport.down(`${params.view}view`);
        }
        Ext.Viewport.setActiveItem(mainView);
        if (initiated === false) {
          let originalLocation = window.location.hash.substring(1);
          // mainView.getController().redirectTo(window.location.hash.substring(1));
          Navigation.redirectTo('main');
          setTimeout(() => {
            Navigation.redirectTo(originalLocation);
          }, 100);
        }
      } else {

      }
    } catch (err) {
      console.error(err);
    }
  },
  mainRoute: (params) => {
    let Navigation = Client.app.getController('Navigation');
    let mainView = Ext.Viewport.down('mainview');
    let initialized = true;
    if (_.isNull(mainView)) {
      initialized = false;
      Ext.Viewport.add({xtype: 'mainview'});
      mainView = Ext.Viewport.down('mainview');
    }
    Ext.Viewport.setActiveItem(mainView);
    if (initialized === false) {
      let originalLocation = window.location.hash.substring(1);
      // mainView.getController().redirectTo(window.location.hash.substring(1));
      Navigation.redirectTo('main');
      setTimeout(() => {
        Navigation.redirectTo(originalLocation);
      }, 100);
    }
  },
  perspectiveRouter: (params) => {
    console.log('Navigating to:', params);
    let Navigation = Client.app.getController('Navigation');
    Navigation.perspectiveTokens[params.perspective] = window.location.hash.substring(1);
    let classExists = Ext.ClassManager.getByAlias(`widget.perspective.${params.perspective}`);
    if (_.isUndefined(classExists)) {
      console.log(`Perspective does not exist!`);
    } else {
      let initialized = true;
      let perspective = Ext.Viewport.down(`perspective\\.${params.perspective}`);
      if (_.isNull(perspective)) {
        perspective = Ext.create({xtype: `perspective.${params.perspective}`});
        initialized = false;
        Ext.Viewport.add(perspective);
      }
      if (initialized === false) {
        perspective.on('show', () => {
          let originalLocation = window.location.hash.substring(1);
          console.log('Reloading route to', originalLocation);
          Navigation.redirectTo(originalLocation, {force: true, replace: true});
          // Ext.util.History.back();
          // // Navigation.redirectTo('');
          // setTimeout(() => {
          //   Ext.util.History.forward();
          //   // Navigation.redirectTo(originalLocation);
          // }, 100);
        }, this, {single: true});
      }
      if (perspective) Ext.Viewport.setActiveItem(perspective);
    }
  },
  onUnmatchedRoute: (token) => {
    let me = this;
    let Navigation = Client.app.getController('Navigation');
    try {
      console.log('Unmatched Route', token);
      if (_.startsWith(token, 'main/')) {
        Navigation.redirectTo('main');
        setTimeout(() => {
          let mainView = Ext.Viewport.down('perspective\\.main');
          mainView.getController().redirectTo(token);
        }, 100);
      } else {
        console.log('Unmatched Route', token);
      }
    } catch (err) {
      console.log(me);
      console.error(err);
    }
  },
  gotoPerspective: (perspective) => {
    let me = this;
    let Navigation = Client.app.getController('Navigation');
    if (!_.isUndefined(Navigation.perspectiveTokens[perspective])) {
      Navigation.redirectTo(Navigation.perspectiveTokens[perspective]);
    } else {
      Navigation.redirectTo(perspective);
    }
  },
  onLaunch: (application) => {

  }
});
