module.exports = function () {
  return function auth(req, res, next) {
    let app = req.app;
    let user = app.registry.getModelByType('User');
    if (req.accessToken) {
      if (!req.user) {
        user.findById(req.accessToken.userId, (uErr, u) => {
          req.user = u;
          next();
        });
      } else {
        next();
      }
    } else {
      next();
    }
  };
};
