/**
 * Created by Nam on 10/26/2017.
 */
'use strict';
const _ = require('lodash');
const debug = require('debug')('loopback:mixin:ext-query');
/**
 * Created by Nam on 9/22/2017.
 */
module.exports = function(Model, options) {
  options = _.merge({
    limitParam: 'limit',
    skipParam: 'start',
    sortParam: 'sort',
  }, options);
  debug('Loaded Options:', options);
  Model.beforeRemote('**', function(ctx, instance, next) {
    let query = ctx.req.query;
    if (query[options.skipParam] && query[options.limitParam]) {
      if (_.isUndefined(ctx.args.filter)) {
        ctx.args.filter = {};
      }
      ctx.args.filter.limit = query[options.limitParam];
      ctx.args.filter.skip = query[options.skipParam];
    }
    if (query[options.sortParam]) {
      let sorters = JSON.parse(query[options.sortParam]);
      ctx.args.filter.order = [];
      _.each(sorters, function(sort) {
        ctx.args.filter.order.push(sort.property + ' ' + sort.direction);
      });
    }
    next();
  });
};
