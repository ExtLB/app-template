import * as _ from 'lodash';
import * as async from 'async';
import * as path from "path";
import * as glob from 'glob';
import {LoopBackApplication} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('server/boot/modules');

export class ModuleLoader {
  detectedModules: any;
  options: any;
  constructor(options: any) {
    let me = this;
    me.detectedModules = [];
    options = _.merge({
      initialize: true,
      patterns: ['node_modules/*/extlb.json', 'node_modules/*/*/extlb.json'],
      filters: {
        package: () => {}
      }
    }, options);
    me.options = options;
    if (options.initialize === true) {
      me.init();
    }
  }
  init() {
    let me = this;
    async.map(me.options.patterns, (pattern: string, cb: (err: Error, files: any) => any) => {
      glob(pattern, {}, (err: Error, files: [string]) => {
        cb(err, files.map((file) => {
          return {
            "extlb.json": file,
            directory: path.dirname(file),
            config: require(`${process.cwd()}${path.sep}${file}`),
            module: require(`${process.cwd()}${path.sep}${path.dirname(file)}`)
          };
        }));
      });
    }, (err: Error, patternResults: any) => {
      if (err) {
        console.error(err);
      } else {
        me.detectedModules = _.concat.apply(me, patternResults);
      }
      if (me.options.callback) {
        me.options.callback();
      }
    });
  }
  client() {
    let me = this;
    return _.filter(me.detectedModules, (dm) => {
      return dm.config.client;
    });
  }
  server() {
    let me = this;
    return _.filter(me.detectedModules, (dm) => {
      return dm.config.server;
    });
  }
}

class Modules {
  app: LoopBackApplication;
  modules: ModuleLoader;
  constructor(app: LoopBackApplication, done: any) {
    let me = this;
    me.app = app;
    log.info('Initializing Modules');
    me.modules = new ModuleLoader({
      initialize: true,
      patterns: ['node_modules/*/extlb.json', 'node_modules/*/*/extlb.json'],
      callback: () => {
        let serverModules: any = me.modules.server();
        let bootLoaders:any = {};
        _.each(serverModules, (mod) => {
          bootLoaders[mod.config.name] = (callback: any) => {
            let blModule = mod.module;
            let bootLoader = new blModule();
            bootLoader.boot(app, callback);
          };
        });
        if (_.keys(bootLoaders).length > 0) {
          async.series(bootLoaders, (err, bootLoader) => {
            if (err) {
              log.error(err);
            } else {
              log.info('Boot Loader:', bootLoader);
            }
            log.info(`Loaded ${serverModules.length} server modules`);
            done();
          });
        } else {
          done();
        }
      }
    });
    (me.app as any).modules = me.modules;
  }
}

module.exports = Modules;
