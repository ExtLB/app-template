'use strict';
/**
 * Created by Nam on 8/27/2017.
 */

let pkg = require('../package.json');
let version = pkg.version.split('.').shift();
let config = {
  restApiRoot: '/api' + (version > 0 ? '/v' + version : ''),
};
switch (process.env.NODE_ENV) {
  case 'production':
    // config.port = 3000;
    config.environment = process.env.NODE_ENV;
    break;
  case 'staging':
    // config.port = 3002;
    config.environment = process.env.NODE_ENV;
    break;
  default:
    config.environment = process.env.NODE_ENV;
}
module.exports = config;
