import {LoopBackApplication, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('server/models/role');

export interface roleInterface extends PersistedModel {
  id: string;
  name: string;
  description: string;
  created: Date;
  modified: Date;
}
export interface role extends PersistedModel {
}

class roleModel {
  app: LoopBackApplication;
  model: role;
  constructor(model: role) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached role Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (role: role) {
  new roleModel(role);
};
