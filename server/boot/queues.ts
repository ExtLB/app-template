import {LoopBackApplication} from "loopback";
import * as Queue from 'bull';
import * as _ from 'lodash';
let log = require('@dosarrest/loopback-component-logger')('server/boot/queues');

class Queues {
  app: LoopBackApplication;
  queues: any = {};
  queueConfigs: any[] = [];
  constructor(app: LoopBackApplication, done: any) {
    let me = this;
    me.app = app;
    me.queueConfigs = app.get('queues');
    if (!_.isUndefined(me.queueConfigs) && _.isObject(me.queueConfigs) && _.keys(me.queueConfigs).length > 0) {
      log.info('Loading Queues');
      (me.app as any).queues = me;
      _.each(me.queueConfigs, (queueConfig: Queue.QueueOptions, queueName: string) => {
        log.info('Loading Queue:', queueName);
        me.queues[queueName] = new Queue(queueName, queueConfig);
      });
    }
    (app as any).queues = me.queues;
    done();
  }
  get(queue: string): Queue.Queue {
    let me = this;
    return me.queues[queue];
  }
}

module.exports = Queues;
