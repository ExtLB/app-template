import * as _ from 'lodash';
import {BootScript} from "@mean-expert/boot-script";
import {AccessToken, LoopBackApplication, User} from "loopback";
import {NextFunction, Request, Response, Router} from "express";
const parseDomain = require('parse-domain');
let log = require('@dosarrest/loopback-component-logger')('server/boot/root');

interface LoopBackRequest extends Request {
  user: User;
  accessToken: AccessToken;
}
interface SenchaLoopInfoResponse {
  configured: boolean;
  defaultToken: string,
  passport: boolean,
  auth: {
    enabled: boolean,
    loginUrl: string,
  },
  perspectives: any;
  localeNamespaces: string[];
  bundles: string[];
  loggedIn: boolean;
  userId?: string;
}

@BootScript()
class Root {
  app: LoopBackApplication;
  router: Router;
  constructor(app: LoopBackApplication) {
    log.debug('Initiated Root Boot Script');
    let me = this;
    me.app = app;
    me.router = Router();
    let authOptions = app.get('auth');
    me.router.get('/info', (req: LoopBackRequest, res: Response, next: NextFunction) => {
      let response: SenchaLoopInfoResponse = {
        configured: true,
        defaultToken: app.get('defaultToken'),
        passport: app.get('passport'),
        auth: {
          enabled: authOptions.enabled,
          loginUrl: authOptions.loginUrl,
        },
        perspectives: _.concat.apply(me, (app as any).modules.client().map((serverModule: any) => { return serverModule.config.perspectives; })),
        localeNamespaces: (app as any).modules.client().map((serverModule: any) => { return serverModule.config.locale; }),
        bundles: _.concat.apply(me, (app as any).modules.client().map((serverModule: any) => { return serverModule.config.bundles; })),
        loggedIn: false,
      };
      response.bundles = _.filter(response.bundles, (bundle) => {
        return !_.isUndefined(bundle);
      });
      if (req.user && req.accessToken) {
        response.loggedIn = true;
        response.userId = (req.user as any).id;
      }
      res.status(200).json(response).end();
    });
    app.use(this.router);
  }
}
module.exports = Root;
// module.exports = function(server) {
  // Install a `/` route that returns server status
  // var router = server.loopback.Router();
  // router.get('/', server.loopback.status());
  // server.use(router);
// };
