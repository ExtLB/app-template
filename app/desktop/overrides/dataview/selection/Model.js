Ext.define('Override.dataview.selection.Model', {
  override: 'Ext.dataview.selection.Model',

  selectWithEventMulti: function(record, e, isSelected) {
    let me = this,
      shift = e.shiftKey,
      ctrl = e.ctrlKey,
      start = shift ? me.selectionStart : null;

    if (shift && start) {
      me.selectRange(start, record, ctrl);
    } else {
      //here
      if(isSelected) me.deselect(record);
      else me.select(record, true);
      //me[isSelected ? 'deselect' : 'select'](record, true);
    }
  }
});
