Ext.define('Client.view.loader.LoaderView', {
  extend: 'Ext.Container',
  xtype: 'loaderview',
  requires: [
    'Ext.Progress'
  ],
  viewModel: {
    type: 'loaderviewmodel'
  },
  controller: {
    type: 'loaderviewcontroller'
  },
  userCls: 'loader-view',
  itemId: 'loaderView',
  reference: 'loaderView',
  layout: {
    type: 'hbox',
    align: 'center',
    pack: 'center'
  },
  items: [{
    xtype: 'progress',
    reference: 'loaderProgress',
    itemId: 'loaderProgress',
    flex: 1,
    bind: {
      value: '{progressValue}',
      text: '{progressText}',
    },
    shadow: true,
  }]
});
