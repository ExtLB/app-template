Ext.define('Client.model.User', {
  extend: 'Ext.data.Model',
  requires: [
    'Ext.data.field.Date',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json'
  ],
  fields: [{
    name: 'id',
    persist: false
  }, {
    name: 'realm'
  }, {
    name: 'username'
  }, {
    name: 'email'
  }, {
    name: 'emailVerified'
  }, {
    name: 'password'
  }, {
    name: 'roles'
  }, {
    name: 'accountId'
  }, {
    name: 'account'
  }]
});
