import {LoopBackApplication, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('server/models/roleMapping');

export interface roleMappingInterface extends PersistedModel {
  id: string;
  principalType: string;
  principalId: string;
}
export interface roleMapping extends PersistedModel {
}

class roleMappingModel {
  app: LoopBackApplication;
  model: roleMapping;
  constructor(model: roleMapping) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached roleMapping Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (roleMapping: roleMapping) {
  new roleMappingModel(roleMapping);
};
