'use strict';
require('ts-node').register();
require('babel-register')({
  presets: [ 'env' ]
});

const _ = require('lodash');
<% if (options.features.indexOf('socketio') !== -1) { %>
const LoopBackSocket = require('loopback-socket');
const loopbackSocket = LoopBackSocket.get('app', 10000);
<% } %>
const mustacheExpress = require('mustache-express');
const i18next = require('i18next');
const i18nextExpressMiddleware = require('i18next-express-middleware');
const i18nextFSBackend = require('i18next-node-fs-backend');
let loopback = require('loopback');
let boot = require('loopback-boot');
let bunyan = require('bunyan');
let rootLogger = bunyan.createLogger({name: 'REST API'});
const path = require('path');
let logger = require('@dosarrest/loopback-component-logger')(rootLogger, {perfLogger: path.resolve('./server/advancedLogger')});
const cookieParser = require('cookie-parser');
<% if (options.features.indexOf('passportjs') !== -1) { %>
const oniyiConfig = require('oniyi-config');
<% } %>

i18next
  .use(i18nextExpressMiddleware.LanguageDetector)
  .use(i18nextFSBackend)
  .init({
    preload: ['en'],
    saveMissing: true,
    backend: {
      loadPath: __dirname + '/locales/{{lng}}/{{ns}}.json',
      addPath: __dirname + '/locales/{{lng}}/{{ns}}.missing.json',
    }
  });
let app = module.exports = loopback();
app.use(i18nextExpressMiddleware.handle(i18next, {
  ignoreRoutes: ['/api'],
}));
app.i18next = i18next;

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', __dirname + '/views');

<% if (options.features.indexOf('passportjs') !== -1) { %>
// Create an instance of PassportConfigurator with the app instance
const PassportConfigurator = require('loopback-component-passport').PassportConfigurator;
let passportConfigurator = new PassportConfigurator(app);
<% } %>

app.start = function() {
  // start the web server
  return app.listen(app.get('port'), function() {
    app.emit('started');
    let baseUrl = app.get('url').replace(/\/$/, '');
    logger.info(`Web server listening at: ${baseUrl}`);
    if (app.get('loopback-component-explorer')) {
      let explorerPath = app.get('loopback-component-explorer').mountPath;
      logger.info(`Browse your REST API at ${baseUrl}${explorerPath}`);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    <% if (options.features.indexOf('socketio') !== -1) { %>
    let socketConfig = app.get('socket');
    if (!_.isUndefined(socketConfig) && !_.isUndefined(socketConfig.serverOptions) && socketConfig.serverOptions.enabled === true) {
      delete socketConfig.serverOptions.enabled;
      app.io = require('socket.io')(app.start(), socketConfig.serverOptions);
      loopbackSocket.start(app.io);
      const parser = cookieParser(app.get('cookieSecret'));
      app.io.use((socket, next) => {
        parser(socket.request, null, next);
      });
    } else {
      app.start();
    }
    <% } else { %>
      app.start();
    <% } %>
  }
});

<% if (options.features.indexOf('passportjs') !== -1) { %>
// Load the provider configurations
let config = {};
try {
  config = oniyiConfig({
    sourceDir: path.join(process.cwd(), 'server', 'authentication'),
    baseName: 'providers'
  });
} catch(err) {
  logger.error('Please configure your passport strategy in `providers.json`.');
  logger.error('Copy `providers.json.template` to `providers.json` and replace the clientID/clientSecret values with your own.');
  process.exit(1);
}
// Initialize passport
let passport = passportConfigurator.init();

// Set up related models
let userModel = app.registry.getModelByType('User');
// logger.info(userModel.definition.name);
// let userModel = app.registry.getModel('User');
let userIdentityModel = app.registry.getModelByType('UserIdentity');
let userCredentialModel = app.registry.getModelByType('UserCredential');
let applicationCredentialModel = app.registry.getModelByType('ApplicationCredential');
passportConfigurator.setupModels({
  userModel: userModel,
  userIdentityModel: userIdentityModel,
  userCredentialModel: userCredentialModel,
  applicationCredentialModel: applicationCredentialModel,
});
// Configure passport strategies for third party auth providers
for(let s in config) {
  let c = config[s];
  c.session = c.session !== false;
  passportConfigurator.configureProvider(s, c);
}
<% } %>
