import {LoopBackApplication, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('server/models/application');

export interface applicationInterface extends PersistedModel {
  id: string;
  realm: string;
  name: string;
  description: string;
  icon: string;
  owner: string;
  collaborators: string[];
  email: string;
  emailVerified: boolean;
  url: string;
  callbackUrls: string[];
  permissions: string[];
  clientKey: string;
  javaScriptKey: string;
  restApiKey: string;
  windowsKey: string;
  masterKey: string;
  pushSettings: {
    apns: {
      production: boolean,
      certData: string,
      keyData: string,
      pushOptions: {
        type: {
          gateway: string,
          port: number
        }
      },
      feedbackOptions: {
        type: {
          gateway: string,
          port: number
        }
      }
    },
    gcm: {
      serverApiKey: string
    }
  };
  authenticationEnabled: boolean;
  anonymousAllowed: boolean;
  authenticationSchemes: {
    scheme: string,
    credential: object;
  }[];
  status: string;
  created: Date;
  modified: Date;
}
export interface application extends PersistedModel {
}

class applicationModel {
  app: LoopBackApplication;
  model: application;
  constructor(model: application) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached application Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (application: application) {
  new applicationModel(application);
};
