import {LoopBackApplication, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('server/models/accessToken');

export interface accessTokenInterface extends PersistedModel {
  id: string;
  ttl: number;
  scopes: string[];
  created: Date;
}
export interface accessToken extends PersistedModel {
}

class accessTokenModel {
  app: LoopBackApplication;
  model: accessToken;
  constructor(model: accessToken) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached accessToken Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (accessToken: accessToken) {
  new accessTokenModel(accessToken);
};
