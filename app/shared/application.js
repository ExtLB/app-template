global._ = require('lodash');
global.async = require('async');
global.IO = require('socket.io-client');
global.ParseDomain = require('parse-domain');
global.i18next = require('i18next');
global.i18nextBrowserLanguageDetector = require('i18next-browser-languagedetector');
global.i18nextXHRBackend = require('i18next-xhr-backend');
global.i18nextMultiLoadBackendAdapter = require('i18next-multiload-backend-adapter');
global.Favico = require('favico.js');
global.Favicon = new Favico({animation: 'slide'});
global.SharedBundle = function(application) {
  let me = this;
  return new Promise((resolve) => {
    i18next.use(i18nextMultiLoadBackendAdapter);
    // i18next.use(i18nextXHRBackend);
    i18next.use(i18nextBrowserLanguageDetector);
    i18next.init({
      load: 'currentOnly',
      preload: ['en-US'],
      ns: ['common'],
      defaultNS: 'common',
      fallbackLng: 'en-US',
      backend: {
        backend: i18nextXHRBackend,
        backendOption: {
          allowMultiLoading: true,
          loadPath: '/api/v1/Languages/{{lng}}/{{ns}}.json',
        }
      },
    }, (err) => {
      console.log('Shared Bundle Initialized');
      resolve(true);
    });
  });
};
