'use strict';
const _ = require('lodash');
const debug = require('debug')('loopback:mixin:count');
/**
 * Created by Nam on 9/22/2017.
 */
module.exports = function(Model, options) {
  Model.getApp(function(err, app) {
    if (err) {
      console.warn(err);
      return;
    }
    options = _.merge({
      relations: true,
      total: true,
      totalProperty: 'total',
      totalDataProperty: 'data',
      totalWhitelist: [],
    }, options);
    debug('Loaded Options:', options);

    function addCounts(ctx, unused, next) {
        // console.log(ctx.methodString);
      if (options.relations === true && options.total === true) {
        injectRelationCounts(ctx, unused, function() {
          injectCounts(ctx, unused, next);
        });
      } else if (options.relations === true && options.total === false) {
        injectRelationCounts(ctx, unused, next);
      } else if (options.relations === false && options.total === true) {
        injectCounts(ctx, unused, next);
      } else {
        next();
      }
      // next();
    }

    function injectRelationCounts(ctx, unused, next) {
      let relations = extractRelationCounts(ctx);
      let resources = ctx.result;
      if (!Array.isArray(resources)) resources = [resources];
      if (!relations.length || !resources.length) {
        return next();
      }

      fillCounts(relations, resources).then(function() {
        return next();
      }, function() {
        return next();
      });
    }
    function extractRelationCounts(ctx) {
      let filter;
      if (!ctx.args || !ctx.args.filter) return [];
      if (typeof ctx.args.filter === 'string') {
        filter = JSON.parse(ctx.args.filter);
      } else {
        filter = ctx.args.filter;
      }
      let relations = filter && filter.counts;
      if (!Array.isArray(relations)) relations = [relations];
      return relations.filter(function(relation) {
        return Model.relations[relation] &&
          Model.relations[relation].type.startsWith('has');
      });
    }
    function fillCounts(relations, resources) {
      return Promise.all(resources.map(function(resource) {
        return Promise.all(relations.map(function(relation) {
          return resource[relation].count().then(function(count) {
            resource[relation + 'Count'] = count;
          });
        }));
      }));
    }

    function injectCounts(ctx, unused, next) {
      try {
        let resources = ctx.result;
        let resultModel = ctx.resultType[0];
        let model = Model.app.models[resultModel];
        if (!Array.isArray(resources)) resources = [resources];
        if (!resources.length) {
          ctx.result = {};
          ctx.result[options.totalProperty] = 0;
          ctx.result[options.totalDataProperty] = [];
          return next();
        }

        if (resultModel !== Model.definition.name) {
          let relatedName = '';
          let relatedModel = {};
          _.each(Model.relations, function(relation, id) {
            if (
              relation.modelTo.definition.name === resultModel &&
              relation.type.startsWith('has')
            ) {
              relatedName = id;
              relatedModel = relation.modelTo;
              return false;
            }
          });
          // console.log(relatedModel);
          // console.log(relatedName);
          if (relatedName !== '') {
            model = ctx.instance[relatedName];
          }
          // model.count(function(cErr, count) {
          //   console.log(count);
          // });
          // ctx.instance[relatedName].count(function(cErr, count) {
          //   console.log(count);
          // });
        }

        let args = ctx.args && ctx.args.filter &&
        typeof ctx.args.filter === 'string' ? JSON.parse(ctx.args.filter) : {};
        args = ctx.args && ctx.args.filter &&
        typeof ctx.args.filter === 'object' ? ctx.args.filter : args;

        if (!_.isUndefined(model)) {
          let filter = args && args.where ? args.where : {};
          totalCount(model, filter, function (err, count) {
            if (!err) {
              ctx.result = {};
              ctx.result[options.totalProperty] = count;
              ctx.result[options.totalDataProperty] = resources;
            } else {
              console.log(err);
              return next(err);
            }

            return next();
          });
        } else {
          next();
        }
      } catch (err) {
        return next();
      }
    }
    function totalCount(model, filter, done) {
      model.count(filter, function(err, count) {
        if (!err) return done(null, count);
        else return done('Unable to count: ' + err);
      });
    }

    if (options.totalWhitelist.length !== 0) {
      _.each(options.totalWhitelist, function(item) {
        // Model.afterRemote('prototype.__get__notifications', addCounts);
        Model.afterRemote(item, addCounts);
      });
    } else {
      Model.afterRemote('**', addCounts);
    }
    // Model.afterRemote('**', (ctx, unused, next) => {
    //   console.log(ctx.methodString);
    //   next();
    // });
  });
};
