'use strict';
require('ts-node').register();
require('babel-register')({
});
const _ = require('lodash');
const async = require('async');
const path = require('path');
const glob = require('glob');
const fs = require('fs');
const jsonfile = require('jsonfile');

class ModuleLoader {
  constructor(options) {
    let me = this;
    me.detectedModules = [];
    options = _.merge({
      initialize: true,
      patterns: ['node_modules/*/package.json'],
      filters: {
        package: () => {}
      }
    }, options);
    me.options = options;
    if (options.initialize === true) {
      me.init();
    }
  }
  init() {
    let me = this;
    async.map(me.options.patterns, (pattern, cb) => {
      glob(pattern, {}, (err, files) => {
        cb(err, files.map((file) => {
          return {
            "extlb.json": file,
            directory: path.dirname(file),
            config: require(`./${file}`)
          };
        }));
      });
    }, (err, patternResults) => {
      if (err) {
        console.error(err);
      } else {
        me.detectedModules = _.concat.apply(me, patternResults);
      }
      if (me.options.callback) {
        me.options.callback();
      }
    });
  }
  client() {
    let me = this;
    return _.filter(me.detectedModules, (dm) => {
      return dm.config.client;
    });
  }
  server() {
    let me = this;
    return _.filter(me.detectedModules, (dm) => {
      return dm.config.server;
    });
  }
}

module.exports = grunt => {
  require('time-grunt')(grunt);
  grunt.initConfig({
    exec: {
      start: {
        command: 'npm start'
      }<% if (options.platforms.indexOf('desktop') !== -1) { %>,
      desktop: {
        command: 'npm run desktop'
      },
      "desktop-v": {
        command: 'npm run desktop-v'
      }<% } if (options.platforms.indexOf('phone') !== -1) { %>,
      phone: {
        command: 'npm run phone'
      },
      "phone-v": {
        command: 'npm run phone-v'
      }<% } %>,
      all: {
        command: 'npm run all'
      },
      "all-v": {
        command: 'npm run all-v'
      }<% if (options.platforms.indexOf('desktop') !== -1) { %>,
      "build-desktop-testing": {
        command: 'npm run build-desktop-testing'
      },
      "build-desktop-production": {
        command: 'npm run build-desktop-production'
      }<% } if (options.platforms.indexOf('phone') !== -1) { %>,
      "build-phone-testing": {
        command: 'npm run build-phone-testing'
      },
      "build-phone-production": {
        command: 'npm run build-phone-production'
      }<% } %>
    }<% if (options.features.indexOf('browserify') !== -1) { %>,
    browserify: {
      <% if (options.platforms.indexOf('desktop') !== -1) { %>desktop: {
        src: [
          'node_modules/*/app/desktop/application.js',
          'node_modules/*/*/app/desktop/application.js',
          'app/desktop/application.js',
        ],
        dest: 'resources/moderndesktop/application.js',
      },
      <% } if (options.platforms.indexOf('phone') !== -1) { %>phone: {
        src: [
          'node_modules/*/app/phone/application.js',
          'node_modules/*/*/app/phone/application.js',
          'app/phone/application.js',
        ],
        dest: 'resources/modernphone/application.js',
      },
      <% } %>shared: {
        src: [
          'node_modules/*/app/shared/application.js',
          'node_modules/*/*/app/shared/application.js',
          'app/shared/application.js',
        ],
        dest: 'resources/shared/application.js',
      },
    }<% } %>,
    copy: {
      <% if (options.platforms.indexOf('desktop') !== -1) { %>"desktop-config": {
        src: 'config/client/desktop.json',
        dest: 'app.json',
      },
      <% } if (options.platforms.indexOf('phone') !== -1) { %>"phone-config": {
        src: 'config/client/phone.json',
        dest: 'app.json',
      },
      <% } %>
    },
    clean: {
      "build": ['client/*'],
      "build-temp": ['client/temp/*'],
      "build-development": ['client/development/*'],
      "build-testing": ['client/testing/*'],
      "build-production": ['client/production/*'],
    },
  });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('loadModules', function() {
    let done = this.async();
    let moduleChecker = new ModuleLoader({
      initialize: true,
      patterns: ['node_modules/*/extlb.json', 'node_modules/*/*/extlb.json'],
      callback: () => {
        let clientModules = moduleChecker.client();
        let serverModules = moduleChecker.server();
        console.log('Client Modules:', clientModules);
        console.log('Server Modules:', serverModules);
        let cssFiles = {
          shared: {
            'resources/shared/htmlcolors.css': true
          }
        };
        <% if (options.platforms.indexOf('desktop') !== -1) { %>cssFiles.desktop = {};<% } %>
        <% if (options.platforms.indexOf('phone') !== -1) { %>cssFiles.phone = {};<% } %>
        let configs = {};
        configs['app.json'] = jsonfile.readFileSync('app.json');
        configs['component-config.json'] = jsonfile.readFileSync('config/server/component-config.json');
        configs['config.json'] = jsonfile.readFileSync('config/server/config.json');
        configs['datasources.json'] = jsonfile.readFileSync('config/server/datasources.json');
        configs['middleware.json'] = jsonfile.readFileSync('config/server/middleware.json');
        configs['model-config.json'] = jsonfile.readFileSync('config/server/model-config.json');
        if (clientModules.length > 0 || serverModules.length > 0) {
          if (clientModules.length > 0) {
            console.log(`Loaded ${clientModules.length} client modules`);
            clientModules.forEach(clientModule => {
              configs['app.json'].classpath.unshift(`${clientModule.directory}/app/\${build.id}/src`);
              configs['app.json'].overrides.unshift(`${clientModule.directory}/app/\${build.id}/overrides`);
              configs['app.json'].sass.etc.push(`${clientModule.directory}/sass/etc/all.scss`);
              configs['app.json'].sass.var.push(`${clientModule.directory}/app/\${build.id}/sass/var.scss`);
              configs['app.json'].sass.src.push(`${clientModule.directory}/app/\${build.id}/sass/src.scss`);
              if (clientModule.config.sharedCSSFiles) {
                clientModule.config.sharedCSSFiles.forEach((sharedCSSFile) => {
                  cssFiles.shared[sharedCSSFile] = true;
                });
              }
              <% if (options.platforms.indexOf('desktop') !== -1) { %>if (clientModule.config.desktopCSSFiles) {
                clientModule.config.desktopCSSFiles.forEach((desktopCSSFile) => {
                  cssFiles.desktop[desktopCSSFile] = true;
                });
              }<% } %>
              <% if (options.platforms.indexOf('phone') !== -1) { %>if (clientModule.config.phoneCSSFiles) {
                clientModule.config.phoneCSSFiles.forEach((phoneCSSFile) => {
                  cssFiles.desktop[phoneCSSFile] = true;
                });
              }<% } %>
            });
          }
          if (serverModules.length > 0) {
            console.log(`Loaded ${clientModules.length} server modules`);
            try {
              serverModules.forEach(serverModule => {
                console.log(`Loading server module ${serverModule.config.name}`);
                let mod = require(serverModule.config.name);
                configs = new mod().install(configs);
              });
            } catch (err) {
              console.error(err);
            }
          }
        }
        jsonfile.writeFileSync('app.json', configs['app.json'], {spaces: 2});
        jsonfile.writeFileSync('server/component-config.json', configs['component-config.json'], {spaces: 2});
        jsonfile.writeFileSync('server/config.json', configs['config.json'], {spaces: 2});
        jsonfile.writeFileSync('server/datasources.json', configs['datasources.json'], {spaces: 2});
        jsonfile.writeFileSync('server/middleware.json', configs['middleware.json'], {spaces: 2});
        jsonfile.writeFileSync('server/model-config.json', configs['model-config.json'], {spaces: 2});
        let sharedCSSfiles = _.keys(cssFiles.shared);
        let sharedCSSContent = '';
        _.each(sharedCSSfiles, (sharedCSSFile) => {
          sharedCSSContent += fs.readFileSync(sharedCSSFile) + '\n';
        });
        fs.writeFileSync('resources/shared/main.css', sharedCSSContent);
        <% if (options.platforms.indexOf('desktop') !== -1) { %>let desktopCSSfiles = _.keys(cssFiles.desktop);
        let desktopCSSContent = '';
        _.each(desktopCSSfiles, (desktopCSSFile) => {
          desktopCSSContent += fs.readFileSync(desktopCSSFile) + '\n';
        });
        fs.writeFileSync('resources/moderndesktop/main.css', desktopCSSContent);
        <% } %>
        <% if (options.platforms.indexOf('phone') !== -1) { %>let phoneCSSfiles = _.keys(cssFiles.phone);
        let phoneCSSContent = '';
        _.each(phoneCSSfiles, (phoneCSSFile) => {
          phoneCSSContent += fs.readFileSync(phoneCSSFile) + '\n';
        });
        fs.writeFileSync('resources/modernphone/main.css', phoneCSSContent);
        <% } %>
        done();
      }
    });
  });

  grunt.registerTask('default', [
    <% if (options.platforms.indexOf('desktop') !== -1) { %>'browserify:desktop', <% } if (options.platforms.indexOf('phone') !== -1) { %>'browserify:phone', <% } %>'browserify:shared'
  ]);
  <% if (options.platforms.indexOf('desktop') !== -1) { %>
  grunt.registerTask('Desktop Mode', ['copy:desktop-config', 'loadModules']);
  <% } if (options.platforms.indexOf('phone') !== -1) { %>
  grunt.registerTask('Phone Mode', ['copy:phone-config', 'loadModules']);
  <% } %>

  grunt.registerTask('Build All', ['Build Testing', 'Build Testing']);

  grunt.registerTask('Build Testing', ['Build Testing Desktop', 'Build Testing Phone']);
  <% if (options.platforms.indexOf('desktop') !== -1) { %>
  grunt.registerTask('Build Testing Desktop', ['Desktop Mode', 'browserify:shared', 'browserify:desktop', 'exec:build-desktop-testing']);
  <% } if (options.platforms.indexOf('phone') !== -1) { %>
  grunt.registerTask('Build Testing Phone', ['Phone Mode', 'browserify:shared', 'browserify:phone', 'exec:build-phone-testing']);
  <% } %>

  grunt.registerTask('Build Production', ['Build Production Desktop', 'Build Production Phone']);
  <% if (options.platforms.indexOf('desktop') !== -1) { %>
  grunt.registerTask('Build Production Desktop', ['Desktop Mode', 'browserify:shared', 'browserify:desktop', 'exec:build-desktop-production']);
  <% } if (options.platforms.indexOf('phone') !== -1) { %>
  grunt.registerTask('Build Production Phone', ['Phone Mode', 'browserify:shared', 'browserify:phone', 'exec:build-phone-production']);
  <% } %>

};
