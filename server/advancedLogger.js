'use strict';

const _ = require('lodash');
const moment = require('moment');
const responseTime = require('@dosarrest/loopback-component-logger/lib/responseTime');

module.exports = function (app, config, logger) {
  app.middleware('initial:before', function (req, res, next) {
    let ended = false;
    let end = res.end;
    let startTime = responseTime();
    let count = 0;
    let url = req.url;
    let method = req.method;

    res.end = function (chunk, encoding) {

      if (ended)
        return;

      ended = true;
      end.call(this, chunk, encoding);

      let output = {
        user: {
          id: null,
          username: 'anonymous'
        }
      };
      output['req.id'] = req.id;
      output['res.id'] = res.id;
      if (req.user) {
        output.user.id = req.user.id;
        output.user.username = req.user.username;
      }
      if (req.accessToken) {
        let expires = moment(req.accessToken.created).add(req.accessToken.ttl, 'ms');
        output.accessToken = req.accessToken.id;
        output.tokenExpires = expires.diff(moment());
      }
      if (!_.isUndefined(req.body) && !_.isNull(req.body) && _.keys(req.body).length > 0) {
        output.body = req.body;
      }
      if (!_.isUndefined(req.params) && !_.isNull(req.params) && _.keys(req.params).length > 0) {
        output.params = req.params;
      }
      let resHeaders = res.getHeaders();
      if (resHeaders.location) {
        output['res.redirectTo'] = resHeaders.location;
      }
      // if (req.remotingContext && req.remotingContext.args) {
      //   output['req.args'] = _.filter(req.remotingContext.args, (arg, argName) => {
      //     switch (argName) {
      //       case 'req':
      //       case 'res':
      //         return false;
      //       default:
      //         return true;
      //     }
      //   });
      // }
      if (req.remotingContext && req.remotingContext.methodString) {
        output['req.methodString'] = req.remotingContext.methodString;
      }
      if (req.remotingContext && req.remotingContext.resultType) {
        output['req.resultType'] = req.remotingContext.resultType;
      }
      // console.log('Req:', _.keys(req));
      // console.log('Req:', req);
      // console.log('Res:', res);
      output['req.originalUrl'] = req.originalUrl;
      output['req.url'] = req.url;
      output['res.statusCode'] = res.statusCode;
      output['res.statusMessage'] = res.statusMessage;
      output.method = method;
      output.APIResponseTime = res.perf || 'NOT_AN_API';
      if (output.APIResponseTime !== 'NOT_AN_API') {
        output['req.url'] = `${req.baseUrl}${req.url}`;
      }
      output.OverallResponseTime = responseTime(startTime);

      logger.info(output);
    };

    next();
  });
};
