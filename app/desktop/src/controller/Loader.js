Ext.define('Client.controller.Loader', {
  extend: 'Ext.app.Controller',
  requires: [
    'Ext.Promise',
    'Ext.Ajax'
  ],
  refs: {
    loaderView: '#loaderView',
  },
  defaultToken: '',
  tasks: [],
  onLaunch: function(application) {
    let me = this;
    let loaderView = me.getLoaderView();
    let lvm = loaderView.getViewModel();
    let Loader = Client.app.getController('Loader');
    let Navigation = Client.app.getController('Navigation');
    let tasks = [];
    lvm.set('progressText', `${i18next.t('LOADER.INITIALIZE_BUNDLES')}...`);
    lvm.set('progressValue', 0.1);
    me.tasks.unshift(
      {
        name: 'LOADER.INITIALIZE_BUNDLES',
        function: me.initializeCoreBundles.bind(me, application)
      },
      {
        name: 'LOADER.SERVER_INFO',
        function: me.loadServerInfo.bind(me, application)
      },
      {
        name: 'LOADER.LOAD_NAMESPACES',
        function: me.loadNamespaces.bind(me, application)
      },
      {
        name: 'LOADER.LOAD_MODULES',
        function: me.initializeModuleBundles.bind(me, application)
      },
      {
        name: 'LOADER.INITIALIZE_PERSPECTIVES',
        function: me.initializePerspectives.bind(me, application)
      }
    );
    let index = 1;
    async.eachSeries(me.tasks, (task, cb) => {
      let progress = (1 / me.tasks) * index;
      lvm.set('progressValue', progress);
      lvm.set('progressText', task.name);
      index++;
      task.function().then(() => {
        cb();
      }).catch((err) => {
        cb(err);
      });
    }, (err) => {
      if (err) {
        console.log(err);
      }
      lvm.set('progressText', `${i18next.t('LOADER.FINISHED')}...`);
      lvm.set('progressValue', 1);
      if (_.isUndefined(application.hash) || _.isNull(application.hash) || application.hash === '') {
        console.log('Updating hash to default');
        application.hash = Navigation.defaultToken;
      }
      if (Loader.fireEvent('completed', true)) {
        me.redirectTo(application.hash);
        Ext.route.Router.resume(true);
      }
    });
    /*
    tasks.initializeCoreBundles = (callback) => {
      me.initializeCoreBundles(application).then(success => {
        callback(null, success);
      }).catch(err => console.error(err));
    };
    tasks.loadServerInfo = (callback) => {
      lvm.set('progressText', `${i18next.t('LOADER.SERVER_INFO')}...`);
      lvm.set('progressValue', 0.2);
      me.loadServerInfo(application).then(success => {
        callback(null, success);
      }).catch(err => console.error(err));
    };
    tasks.loadNamespaces = (callback) => {
      lvm.set('progressText', `${i18next.t('LOADER.LOAD_NAMESPACES')}...`);
      lvm.set('progressValue', 0.4);
      me.loadNamespaces(application).then(success => {
        callback(null, success);
      }).catch(err => {
        console.error(err);
        callback(err);
      });
    };
    tasks.initializeModuleBundles = (callback) => {
      lvm.set('progressText', `${i18next.t('LOADER.LOAD_MODULES')}...`);
      lvm.set('progressValue', 0.6);
      me.initializeModuleBundles(application).then(success => {
        callback(null, success);
      }).catch(err => {
        console.error(err);
        callback(err);
      });
    };
    tasks.checkAuthentication = (callback) => {
      if (Loader.serverInfo.auth.enabled === true) {
        lvm.set('progressText', `${i18next.t('LOADER.AUTHENTICATING')}...`);
        lvm.set('progressValue', 0.8);
        Client.app.getController('Authentication').check().then((success) => {
          console.log('Authentication Check:', success);
          callback(null, success);
        }).catch(err => {
          console.log('Authentication Failure');
          callback(err);
        });
      } else {
        callback(null);
      }
    };
    tasks.initializePerspectives = (callback) => {
      lvm.set('progressText', `${i18next.t('LOADER.INITIALIZE_PERSPECTIVES')}...`);
      lvm.set('progressValue', 0.9);
      me.initializePerspectives(application).then(success => {
        callback(null, success);
      }).catch(err => {
        console.error(err);
        callback(err);
      });
    };
    async.series(tasks, (err, results) => {
      lvm.set('progressText', `${i18next.t('LOADER.FINISHED')}...`);
      lvm.set('progressValue', 1);
      Loader.fireEvent('completed', true);
      if (_.isUndefined(application.hash) || _.isNull(application.hash) || application.hash === '') {
        console.log('Updating hash to default');
        application.hash = Navigation.defaultToken;
      }
      console.log('Resuming routing and reloading', application.hash);
      Ext.route.Router.resume(true);
      if (Loader.serverInfo.auth.enabled === true) {
        if (Loader.serverInfo.loggedIn === true) {
          console.log('Me2 Hash:', application.hash);
          me.redirectTo(application.hash);
        } else {
          if (Loader.serverInfo.auth.loginUrl.startsWith('#')) {
            me.redirectTo(Loader.serverInfo.auth.loginUrl);
          } else {
            window.location.href = Loader.serverInfo.auth.loginUrl;
          }
        }
      }
    });
    */
  },
  initializeCoreBundles: (application) => {
    let me = this;
    return SharedBundle(application).then(success => {
      return DesktopBundle(application);
    }).then(success => {
      return true;
    });
  },
  loadServerInfo: (application) => {
    let Loader = Client.app.getController('Loader');
    let Navigation = Client.app.getController('Navigation');
    return Ext.Ajax.request({
      url: '/info',
    }).then(response => {
      Loader.serverInfo = Ext.decode(response.responseText);
      Navigation.defaultToken = Loader.serverInfo.defaultToken;
      return Loader.serverInfo;
    });
  },
  loadNamespaces: (application) => {
    let Loader = Client.app.getController('Loader');
    return new Promise((resolve, reject) => {
      i18next.loadNamespaces(Loader.serverInfo.localeNamespaces, (err, t) => {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          resolve(true);
        }
      });
    });
  },
  initializeModuleBundles: (application) => {
    let Loader = Client.app.getController('Loader');
    return new Promise((resolve) => {
      _.each(Loader.serverInfo.bundles, (bundleName) => {
        if (_.isString(bundleName)) {
          window[bundleName](application);
        }
      });
      resolve(true);
    });
  },
  initializePerspectives: (application) => {
    let me = this;
    let Loader = Client.app.getController('Loader');
    return new Promise((resolve) => {
      _.each(Loader.serverInfo.perspectives, (perspective) => {
        Ext.Viewport.add(Ext.create({xtype: `perspective.${perspective}`}));
      });
      resolve(true);
    });
  },
  init: function (application) {
    let me = this;
    let Navigation = Client.app.getController('Navigation');
    Ext.route.Router.suspend();
    console.log('Location Hash:', window.location.hash, (window.location.hash.length > 0));
    if (window.location.hash.length > 0) {
      application.hash = window.location.hash.substring(1);
    } else {
      application.hash = Navigation.defaultToken;
    }
    console.log('Me1 Hash:', application.hash);
    window.location.hash = '';
    Ext.apply(Ext.util.Format, {
      translate: function (value) {
        return i18next.t(value);
      },
    });
  }
});
