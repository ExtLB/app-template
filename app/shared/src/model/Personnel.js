Ext.define('Client.model.Personnel', {
	extend: 'Client.model.Base',
	fields: [
		'name', 'email', 'phone'
	]
});
