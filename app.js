Ext.application({
	extend: 'Client.Application',
	name: 'Client',
  controllers: [
    'Loader', 'Navigation', <% if (options.features.indexOf('auth') !== -1) { %>, 'Authentication'<% } %><% if (options.features.indexOf('socketio') !== -1) { %>, 'Socket'<% } %>
  ]
});
