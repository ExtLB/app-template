Ext.define('Client.controller.Socket', {
  extend: 'Ext.app.Controller',
  requires: [
    'Ext.Progress',
    'Ext.Promise',
    'Ext.Ajax'
  ],
  refs: {
    loaderView: '#loaderView',
  },
  authenticated: false,
  watches: {},
  init: function (application) {
    let me = this;
    let Socket = Client.app.getController('Socket');
    Socket.socketLoader = Ext.create({
      xtype: 'dialog',
      title: 'Connecting',
      closable: false,
      maximizable: false,
      modal: true,
      layout: {
        type: 'vbox'
      },
      items: [
        {
          xtype: 'container',
          html: 'Connecting to WebSocket'
        },
        {
          xtype: 'progress',
          value: 0.5
        }
      ]
    });
    let Loader = Client.app.getController('Loader');
    Loader.on('completed', () => {
      if (Loader.serverInfo.auth.enabled === true) {
        let Authentication = Client.app.getController('Authentication');
        if (Authentication.loggedIn === true) {
          Socket.open();
        }
        Authentication.on('login', () => {
          Socket.open();
        });
        Authentication.on('logout', () => {
          Socket.watches = {};
          Socket.socket.close();
          me.socketLoader.hide();
          Socket.socket = null;
        });
      } else {
        Socket.open();
      }
    });
  },
  open: function() {
    let me = this;
    let Loader = Client.app.getController('Loader');
    me.socketLoader.show();
    if (_.isUndefined(me.socket) || _.isNull(me.socket)) {
      me.socket = IO({
        autoConnect: false,
        reconnectionAttempts: 1
      });
      me.socket.on('logout', () => {
        let Authentication = Client.app.getController('Authentication');
        Authentication.logout().then(() => {
        }).catch((logoutErr) => {
        });
      });
      me.socket.on('connect', () => {
        // send credentials
        console.log('Connected');
        me.fireEvent('connect', me.socket);
        if (Loader.serverInfo.auth.enabled === true) {
          me.socket.emit('authentication', {token: 'unknown'});
        }
        Ext.each(Object.keys(me.watches), (wKey) => {
          let sWKey = wKey.split('_*_');
          if (me.watches[wKey] > 0) {
            if (sWKey.length === 2) {
              me.socket.emit('watch', sWKey[0], sWKey[1]);
            } else {
              me.socket.emit('watch', sWKey[0]);
            }
          }
        });
        me.socketLoader.hide();
      });
      me.socket.on('reconnect', () => {
        me.socketLoader.hide();
        if (Loader.serverInfo.auth.enabled === true) {
          me.socket.emit('authentication', {token: 'unknown'});
        }
        Ext.toast('WebSocket Reconnected!');
        Ext.each(Object.keys(me.watches), (wKey) => {
          let sWKey = wKey.split('_*_');
          if (me.watches[wKey] > 0) {
            if (sWKey.length === 2) {
              me.socket.emit('watch', sWKey[0], sWKey[1]);
            } else {
              me.socket.emit('watch', sWKey[0]);
            }
          }
        });
      });
      me.socket.on('reconnect_failed', () => {
        me.socketLoader.hide();
        console.log('Reconnection Failed');
        let buttons = {
          reconnect: {
            text: 'Reconnect',
            ui: 'raised',
            handler: function (btn, evt) {
              me.socket.connect();
              dialog.destroy();
              me.socketLoader.show();
            }
          }
        };
        if (Loader.serverInfo.auth.enabled === true) {
          buttons.logout = {
            text: 'Log Out',
            ui: 'raised',
            handler: function (btn, evt) {
              me.socketLoader.hide();
              let Authentication = Client.app.getController('Authentication');
              Authentication.logout().then(() => {
                dialog.destroy();
              }).catch(() => {
                dialog.destroy();
              });
            }
          }
        }
        let dialog = Ext.create({
          xtype: 'dialog',
          title: 'Connection Error',
          closable: false,
          maximizable: false,
          modal: true,
          html: 'You have become disconnected from the server.',
          buttons: buttons
        });
        dialog.show();
      });
      me.socket.on('disconnect', (reason) => {
        me.socketLoader.show();
        console.log('Disconnected from SocketIO:', reason);
        Ext.toast('Disconnected from WebSocket!');
      });

      if (Loader.serverInfo.auth.enabled === true) {
        me.socket.on('authenticated', () => {
          me.authenticated = true;
          me.fireEvent('authorized');
          console.log('Authenticated');
          me.socketLoader.hide();
          // socket authenticated
        });

        me.socket.on('unauthorized', () => {
          me.authenticated = false;
          me.fireEvent('unauthorized');
          me.socket.close();
          console.log('Unauthenticated');
          if (Loader.serverInfo.auth.loginUrl.startsWith('#')) {
            me.redirectTo(Loader.serverInfo.auth.loginUrl.substr(1));
          } else {
            window.location.href = Loader.serverInfo.auth.loginUrl;
            return false;
          }
          me.socketLoader.hide();
          // socket failed authentication
        });
      }
    }
    me.socket.open((err) => {
      if (err) {
        me.fireEvent('connectError', err);
        me.socketLoader.hide();
      }
    });
  },
  close: function() {
    let me = this;
    me.socket.close();
    me.socketLoader.hide();
  },
  watch: function (component, item) {
    let me = this;
    if (item === undefined) {
      if (me.watches[component] === undefined) {
        me.watches[component] = 1;
      } else {
        me.watches[component]++;
      }
      if (me.watches[component] === 1) {
        me.socket.emit('watch', component);
      }
    } else {
      if (me.watches[component + '_*_' + item] === undefined) {
        me.watches[component + '_*_' + item] = 1;
      } else {
        me.watches[component + '_*_' + item]++;
      }
      if (me.watches[component + '_*_' + item] === 1) {
        me.socket.emit('watch', component, item);
      }
    }
  },
  unwatch: function (component, item) {
    let me = this;
    if (item === undefined) {
      if (me.watches[component] !== undefined) {
        me.watches[component]--;
      }
      if (me.watches[component] === 0) {
        me.socket.emit('unwatch', component);
      }
    } else {
      if (me.watches[component + '_*_' + item] !== undefined) {
        me.watches[component + '_*_' + item]--;
      }
      if (me.watches[component + '_*_' + item] === 0) {
        me.socket.emit('unwatch', component, item);
      }
    }
  },
  clearWatches: function() {
    let me = this;
    Ext.each(Object.keys(me.watches), (wKey) => {
      let sWKey = wKey.split('_*_');
      if (sWKey.length === 2) {
        me.socket.emit('unwatch', sWKey[0], sWKey[1]);
      } else {
        me.socket.emit('unwatch', sWKey[0]);
      }
    });
    me.watches = {};
  }
});
