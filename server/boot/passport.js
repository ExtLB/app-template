'use strict';
import * as _ from 'lodash';
import parseDomain from 'parse-domain'
let log = require('@dosarrest/loopback-component-logger')('server/boot/passport');

module.exports = function(app, cb) {
  /*
   * The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.
   * `app.datasources.YourDataSource`). See
   * https://loopback.io/doc/en/lb3/Working-with-LoopBack-objects.html
   * for more info.
   */

  let userModel = app.registry.getModelByType('User');
  let accessTokenModel = app.registry.getModelByType('AccessToken');
  _.each(app.models, (model, modelName) => {
    switch (modelName) {
      case 'UserCredential':
        model.belongsTo(userModel, {as: 'user', foreignKey: 'userId'});
        break;
      case 'UserIdentity':
        model.belongsTo(userModel, {as: 'user', foreignKey: 'userId'});
        break;
    }
  });
  let router = app.loopback.Router();
  router.get('/auth/success', (req, res, next) => {
    if (!_.isUndefined(req.user) && !_.isUndefined(req.user.id)) {
      if (_.isNull(req.accessToken) || req.accessToken.userId !== req.user.id) {
        /**
         * Generate new access token
         */
        accessTokenModel.createAccessTokenId(function(genATErr, newToken) {
          log.info('New Token for ' + req.user.id + ':', newToken);
          req.user.accessTokens.create({
            id: newToken,
          }, function(atErr, at) {
            res.cookie('accessToken', at.id, {signed: true, maxAge: at.ttl * 1000});
            res.status(200).json({success: true}).end();
          });
        });
      } else {
        res.status(200).json({success: true}).end();
      }

    } else {
      // console.log(req);
      res.status(200).json({success: false}).end();
    }
  });
  router.get('/auth/failure', (req, res, next) => {
    res.status(200).json({success: false}).end();
  });
  router.get('/auth/check', (req, res, next) => {
    let success = true;
    if (_.isUndefined(req.user) && _.isNull(req.accessToken)) {
      success = false;
    } else if (_.isUndefined(req.user) && !_.isNull(req.accessToken)) {
      success = false;
    }
    res.status(200).json({success: success}).end();
    // res.status(200).render('success', {t: app.i18next, language: res.locals.language, success: success});
  });
  app.use(router);
  userModel.afterRemote('logout', (ctx, modelInstanceOrNext, next) => {
    ctx.res.clearCookie('connect.sid');
    ctx.res.clearCookie('accessToken');
    next();
  });
  process.nextTick(cb); // Remove if you pass `cb` to an async function yourself
};
