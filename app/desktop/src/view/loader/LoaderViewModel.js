 Ext.define('Client.view.loader.LoaderViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.loaderviewmodel',
	data: {
		progressText: 'Loading...',
    progressValue: 0.0,
	}
});
