'use strict';

module.exports = {
  "local": {
    "provider": "local",
    "module": "passport-local",
    "usernameField": "email",
    "passwordField": "password",
    "authPath": "/auth/local",
    "successRedirect": "/auth/success",
    "failureRedirect": "/auth/failure"
  }
};
