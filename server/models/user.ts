import {LoopBackApplication, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('server/models/user');

export interface UserInterface extends PersistedModel {
  id: string;
  username: string;
  email: string;
  emailVerified: boolean;
  password: string;
  realm: string;
}
export interface User extends PersistedModel {
}

class UserModel {
  app: LoopBackApplication;
  model: User;
  constructor(model: User) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached user Model to Application');
      });
      (me.model as any).afterRemote('login', (context: any, remoteMethodOutput: any, next: Function) => {
        if (context.result.id) {
          context.res.cookie('accessToken', context.result.id, {signed: true, maxAge: context.result.ttl * 1000});
        }
        next();
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (user: User) {
  new UserModel(user);
};
